/*
Great work here, Austin.  This looks and works fantastic!
You've followed all the business rules and named everything
correctly, and have the comments I needed to see as well.
Great looking form.
20/20
*/

function loadProvinces() {

	// Make shorthands for selectors & initiate array
	var provArray = ['Alberta', 'British Columbia', 'Manitoba', 'New Brunswick', 'Newfoundland and Labrador', 'Northwest Territories', 'Nova Scotia', 'Nunavut', 'Ontario', 'Prince Edward Island', 'Quebec', 'Saskatchewan', 'Yukon Territory'];
	var cboProv;
	var cboProv = document.getElementById('cboProv');
	var defaultOpt = document.createElement('option');


	// Set and create default option
	defaultOpt.value = '';
	defaultOpt.innerHTML = '-Select-';
	cboProv.appendChild(defaultOpt);

	// Create options for items in array
	for (var i = 0; i < provArray.length; i++) {
	    var opt = document.createElement('option');
	    opt.value = provArray[i];
	    opt.innerHTML = provArray[i];
	    cboProv.appendChild(opt);
	}
}

function validateForm() {

	var cboProv = document.getElementById('cboProv');
	var txtName = document.getElementById('txtName');

	// Verify if user inputed any name
 	if (txtName.value === '') {
	    alert('Please type in your name!');
	    txtName.focus();
	    return;
	// Verify if user inputed email
	} else if (txtEmail.value === '') {
	    alert('Please type in your email!');
	    txtEmail.focus();
	    return;
	// Check if province selection was made by user
	} else if (cboProv.selectedIndex === 0) {
	    alert('Please select a province!');
	    cboProv.focus();
	    return;
	// If form was properly filled out, display confirmation message
	} else {
	    alert('Thanks, the form was filled out correctly!');
	}
}
