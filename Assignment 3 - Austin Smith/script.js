// This looks and works great, Austin.
// Your RPSD game also keeps score over time
// (something that wasn't required) and it
// has the comments and functions named as
// required.  Nothing wrong here.
// 20/20

var gamePieces;
var userMove
var computerMove;
var playerScore = 0;
var compScore = 0;

function getRandomGamePiece(gamePieces) {
	// get random number which corresponds to array
	var rndm = Math.floor((Math.random() * gamePieces) + 0);
	return rndm;
}

function startGame() {
		// Prompt user to make a move according to the listed choices
		userMove = prompt('Make A Move! - type in either "Rock", "Paper, "Scissors" or "Dynamite"');
		// Initiate Array of game pieces
		gamePieces = ['Rock','Paper','Scissors','Dynamite'];
		// Choose a random game piece for the computer
		computerMove = gamePieces[getRandomGamePiece(gamePieces.length)]
		// Run function which validates who one
		whoWins();
	}

function whoWins() {

	document.getElementById('playerScore').innerHTML = playerScore;
	document.getElementById('compScore').innerHTML = compScore;

	var results = '';
	// determine whether computer or user wins
	if(userMove == 'Rock' && computerMove == 'Paper'){
		results = ('You lost! The computer chose: ' + computerMove);
		compScore++;
	}
	else if(userMove == 'Rock' && computerMove == 'Dynamite'){
		results = ('You lost! The computer chose: ' + computerMove);
		compScore++;
	}
	else if(userMove == 'Rock' && computerMove == 'Scissors'){
		results = ('You won! The computer chose: ' + computerMove);
		playerScore++;
	}
	else if(userMove == 'Paper' && computerMove == 'Dynamite'){
		results = ('You lost! The computer chose: ' + computerMove);
		compScore++;
	}
	else if(userMove == 'Paper' && computerMove == 'Scissors'){
		results = ('You lost! The computer chose: ' + computerMove);
		compScore++;
	}
	else if(userMove == 'Scissors' && computerMove == 'Dynamite'){
		results = ('You won! The computer chose: ' + computerMove);
		playerScore++;
	}
	else if(computerMove == 'Rock' && userMove == 'Paper'){
		results = ('You won! The computer chose: ' + computerMove);
		playerScore++;
	}
	else if(computerMove == 'Rock' && userMove == 'Dynamite'){
		results = ('You won! The computer chose: ' + computerMove);
		playerScore++;
	}
	else if(computerMove == 'Rock' && userMove == 'Scissors'){
		results = ('You lost! The computer chose: ' + computerMove);
		compScore++;
	}
	else if(computerMove == 'Paper' && userMove == 'Dynamite'){
		results = ('You won! The computer chose: ' + computerMove);
		playerScore++;
	}
	else if(computerMove == 'Paper' && userMove == 'Scissors'){
		results = ('You Won! The computer chose: ' + computerMove);
		playerScore++;
	}
	else if(computerMove == 'Scissors' && userMove == 'Dynamite'){
		results = ('You lost! The computer chose: ' + computerMove);
		compScore++;
	}
	else if(computerMove == userMove){
		results = ('You tied! The computer also chose: ' + computerMove);
		compScore++;
		playerScore++;
	}
	else {
		results = ('Invalid move, Please follow the exact case in the alert.');
	}
	// Write results to a div in the html doc
	$(document).ready(function(){
	  $('#results').text(results);
	});
}
